# Arys

## If pnpm is not installed

```shell
npm install pnpm -g
```

## How to run

```shell
pnpm install
pnpm run dev
```

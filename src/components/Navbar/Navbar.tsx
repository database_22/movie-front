import {Group, Navbar as MantineNavbar, ScrollArea, useMantineTheme} from '@mantine/core'
import {useMediaQuery} from '@mantine/hooks'
import clsx from 'clsx'
import {observer} from 'mobx-react-lite'
import {FC, useEffect} from 'react'
import {routes} from '../../routes'
import {appState} from '../../state'
import {ButtonLink} from '../ButtonLink'
import s from './Navbar.module.css'

export const Navbar: FC = observer(() => {
	const theme = useMantineTheme()
	const isTablet = useMediaQuery(`(max-width: ${theme.breakpoints.sm}px)`)

	useEffect(() => {
		return () => {
			if (isTablet) {
				appState.closeNavbar()
			}
		}
	}, [isTablet])

	return (appState.isNavbarOpen || isTablet) ? (
		<MantineNavbar hidden={!appState.isNavbarOpen} p='md' className={clsx(isTablet && s.tabletNavbar)}
			hiddenBreakpoint='sm' width={{sm: 200, lg: 300}}
		>
			<MantineNavbar.Section grow component={ScrollArea}>
				<Group direction='column'>
					<ButtonLink path={routes.movieOfTheDay.path} fullWidth>
						Movie of the day
					</ButtonLink>
					<ButtonLink path={routes.movieByMood.path} fullWidth>
						By mood
					</ButtonLink>
					<ButtonLink path={routes.randomMovie.path} fullWidth>
						Random
					</ButtonLink>
					<ButtonLink path={routes.topMovies.path} fullWidth>
						Top
					</ButtonLink>
					<ButtonLink path={routes.undiscoveredMovies.path} fullWidth>
						Undiscovered
					</ButtonLink>
				</Group>
			</MantineNavbar.Section>
		</MantineNavbar>
	) : null
})

import {Burger, Group, Header as MantineHeader, useMantineTheme} from '@mantine/core'
import {useMediaQuery} from '@mantine/hooks'
import {observer} from 'mobx-react-lite'
import {FC} from 'react'
import {ButtonLink, DarkModeButton} from '..'
import {routes} from '../../routes'
import {appState} from '../../state'
import s from './Header.module.css'

interface Props {
	toggleColorScheme: () => void
}

export const Header: FC<Props> = observer(({toggleColorScheme}) => {
	const theme = useMantineTheme()
	const isTablet = useMediaQuery(`(max-width: ${theme.breakpoints.sm}px)`)

	const onBurgerClick = () => {
		appState.toggleNavbar()
	}

	return (
		<MantineHeader
			height={70}
			py='md'
			px={isTablet ? 'xs' : 'md'}
		>
			<Group position='apart'>
				{isTablet ? (
					<Burger
						opened={appState.isNavbarOpen}
						onClick={onBurgerClick}
						size={26}
						color='gray'
					/>
				) : (
					<ButtonLink
						path='/movie-of-the-day'
						px={0}
						className={s.logoWrapper}
						withoutColor
					>
						Logo
					</ButtonLink>
				)}
				{!isTablet && (
					<div>
						<ButtonLink path={routes.movieOfTheDay.path}>
							Movie of the day
						</ButtonLink>
						<ButtonLink path={routes.movieByMood.path}>
							By mood
						</ButtonLink>
						<ButtonLink path={routes.randomMovie.path}>
							Random
						</ButtonLink>
						<ButtonLink path={routes.topMovies.path}>
							Top
						</ButtonLink>
						<ButtonLink path={routes.undiscoveredMovies.path}>
							Undiscovered
						</ButtonLink>
					</div>
				)}
				<DarkModeButton toggle={toggleColorScheme}/>
			</Group>
		</MantineHeader>
	)
})

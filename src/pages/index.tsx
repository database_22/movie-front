import {MovieOfTheDayPage} from './MovieOfTheDayPage'
import {MoviesByMoodPage} from './MoviesByMoodPage'
import {Page} from './Page'
import {RandomMoviePage} from './RandomMoviePage'
import {TopMoviesPage} from './TopMoviesPage'
import {UndiscoveredMoviePage} from './UndiscoveredMoviePage'

export {Page, MovieOfTheDayPage, MoviesByMoodPage, RandomMoviePage, UndiscoveredMoviePage, TopMoviesPage}

import {lazy} from 'react'
import {MovieOfTheDayPage} from './pages'
import {IRoute} from './types'

export const routes: Record<string, IRoute> = {
	movieOfTheDay: {
		path: '/movie-of-the-day',
		title: 'Movie of the day',
		Component: lazy(() => import('./pages').then(({MovieOfTheDayPage}) => ({default: MovieOfTheDayPage}))),
	},
	movieByMood: {
		path: '/by-mood',
		title: 'Movies by mood',
		Component: lazy(() => import('./pages').then(({MoviesByMoodPage}) => ({default: MoviesByMoodPage}))),
	},
	randomMovie: {
		path: '/random',
		title: 'Random movie',
		Component: lazy(() => import('./pages').then(({RandomMoviePage}) => ({default: RandomMoviePage}))),
	},
	topMovies: {
		path: '/top',
		title: 'Top movies',
		Component: lazy(() => import('./pages').then(({TopMoviesPage}) => ({default: TopMoviesPage}))),
	},
	undiscoveredMovies: {
		path: '/undiscovered',
		title: 'Undiscovered movies',
		Component: lazy(() => import('./pages').then(({UndiscoveredMoviePage}) => ({default: UndiscoveredMoviePage}))),
	},
}

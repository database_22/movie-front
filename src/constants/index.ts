export const BASE_URL = 'http://localhost:10000/'

export const POSTER_BASE_URL = 'https://image.tmdb.org/t/p/original/'

export const LOCAL_STORAGE_KEYS = {
	colorScheme: 'color-scheme',
	userId: 'user-id',
}
